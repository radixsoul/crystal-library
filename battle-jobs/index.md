# Battle Jobs

In this category you can find all information regarding FFXIV Battle jobs, including how to unlock, questions, and general gameplay content.

## Commonly asked questions

*You can find more questions in the menu on the left on Desktop, or clicking the menu button on the top left on mobile.*

- [I am Level 30 why no job advancements](I-am-level-30-why-no-job-advancement-quest.md)
- [Why minimize Battle Macros usage](Why-minimize-Battle-Macros-usage.md)
- [What is GCD and oGCD?](what-is-GCD-oGCD.md)
