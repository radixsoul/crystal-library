---
authors:
        - Robinaite
tags:
    - RDM
---
# Red Mage

## What is a Red Mage?

Red mages are the masters of balancing Black and White magic, going in and out with their rapier.

Red Mage ({{ RDM }}) is a Magic Ranged DPS which is highly mobile. It tries to gather black and white magic, while keeping it balanced to dish out a strong combo.

Red Mage uses a Rapier as their weapon.

## How to unlock Red Mage

1. Own the Stormblood Expansion
2. Level any other Job to level 50
3. Start the quest [Taking the Red](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/3870f2d3cf2/){target=_blank} in Ul'dah
4. Red Mage starts at level 50

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("qRRgCiWJVAM")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to [https://www.akhmorning.com/jobs/rdm/](https://www.akhmorning.com/jobs/rdm/){target=_blank}

## Commonly asked questions
