---
authors:
        - Robinaite
tags:
    - SMN
    - ACN
---
# Summoner

## What is a Summoner?

Summoners are mages who had not only the power to summon the primals, but also the means to transmute the primals' essences, thus binding them to their will.

Summoner ({{ SMN }}) is a Magic Ranged DPS which focuses on pets. In this case Primals. It also shares levels with {{ SCH }}.

Summoner uses a book as their weapon.

Scholar and Summoner share the same levels, leveling one, levels the other.

## How to unlock Summoner

1. Unlock Arcanist in Limsa Lominsa, which starts at level 1 ([Way of the Arcanist](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/4100af4d4b8/){target=_blank})
2. Level up Arcanist to level 30
3. Unlock {{ SMN }} through doing the job quests. ([Austerites of Flame](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/4bb5af1e8a9/){target=_blank})

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("XzBuQcfUdzU")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to [https://www.akhmorning.com/jobs/smn/](https://www.akhmorning.com/jobs/smn/){target=_blank}

## Commonly asked questions
