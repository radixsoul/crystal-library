---
authors:
        - Robinaite
        - Reiko
tags:
    - MCH
---
# Machinist

## What is a Machinist?

Following the example of Cid Garlond, who has demonstrated the potency of magitek, the Skysteel Manufactory works tirelessly on the development of advanced armaments. As new and devastating weapons are brought to the fray, a new class of champion arises to wield them: the machinist.

Machinist ({{ MCH }}) is a Physical Ranged DPS which is highly mobile. Contrary to the other two physical DPS, this one focuses more on doing damage, than giving buffs.

Machinist uses a gun as their weapon.

## How to unlock Machinist

1. Get to Ishgard in the Heavensward Expansion
2. Start the quest [Savior of Skysteel](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/8b4a18330a8/){target=_blank} in Foundation
3. Machinist starts at level 30

!!! info "Unlock info"
    You NEED to have finished the whole of {{ ARR }}, including patch content to be able to unlock it.

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("wlsdao9KlCU")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}

## Commonly asked questions

### What is Machinist's playstyle

Unlike the other physical ranged DPS, Machinist has no buffs to give nor any RNG in their rotation. It is a pure damage dealer revolving around using two separate resource gauges, timing their strong GCDs, and successfully weaving a lot of off global cooldowns. MCH is one of the highest APM jobs in the game and is the least bad ping friendly because of its Hypercharge phase that reduces your GCD to 1.5 sec but also requires you to weave in that time.

### What kind of stats does MCH want?

Like all classes, while leveling you simply want to prioritize item level above all else as your main stat (Dexterity in MCH's case) is always going to have the biggest impact. If you are building toward your BiS the priority is Crit > Direct Hit = Det > Sks. Despite what seems to be the common wisdom, MCH can function with a fair amount of skill speed and in the past some BiS sets have included it. Current tier however, ideal is to have 0 skill speed but if you have some it's not the end of the world it's just the least helpful of all the other stats.

### Can I double weave during Hypercharge?

No, even on the best ping it is not possible to fit in 2 oGCDs between each Heat Blast during Hypercharge without delaying your next Heat Blast. You should simply be rotating back and forth between weaving Ricochet and Gauss Round (start with which ever had more charges when Hypercharge began)
### I know MCH is hard with bad ping, but how bad can mine be but still be okay?

"The golden standard is hitting 5 Heat Blasts in Hypercharge and 1 extra GCD for Wildfire for a total of 6 in Wildfire while weaving a Gauss Round or Ricochet between every attack for raid buff gains. If you can hit the 5+1 GCDs while weaving only 2 times, you can avoid overcapping which is passable but not optimal. If you can't weave at all or if you miss GCDs in Hypercharge or Wildfire, then your performance will be suboptimal" –D//K, The Balance discord

### When should I use Rook/Queen?

If you want to get into the nitty gritty of the timings consult the guide posted on The Balance but for a simple rule of thumb, you want to use it every minute. This will allow it to naturally line up with most all major raid buffs. An easy way to do this is to remember to use it every time you use Reassemble+Drill. This can be shifted to 90 sec windows if you have a Monk and only 3 minute raid buffs in addition to Monk's. As the potency per gauge is constant however, you are free to adjust this to your group's timings in either direction as long as you don't overcap on your gauge.

### How many GCDs should I be getting during Wildfire? Does Rook/Queen add damage to Wildfire?

6, 5 (or 4 if applying after the first Heat Blast) during Hypercharge and 1 (or 2) regular GCDs after. If timing is tight be careful making Drill your last GCD as its damage takes longer than normal to go out, especially if you're starting Wildfire after your first Heat Blast. And then no, Rook/Queen does not add damage to Wildfire. Also, Wildfire cannot be a crit or direct hit, so don't be looking for one and think you're just very unlucky.

### Do buffs affect Wildfire and/or Rook/Queen?

Yes. Like DoTs, any buffs are snapshotted when Wildfire is first applied. Rook/Queen mirrors any buffs applied to you in real time except Dragon Sight which is not applied to Rook/Queen damage.

### When do I use Hypercharge?

If the following conditions are met:

1. Both Drill and Air Anchor have more than 8 seconds remaining in their cooldown. You do not want to delay either of these if at all possible
2. You will not break your combo. You can do a full Hypercharge and *either* Drill or Air Anchor before or after but not both and still have your combo going.
3. You have 1 or fewer charges of Ricochet and Gauss Round off cooldown
4. You're about to use Wildfire or will still have enough gauge to do so when it is next off cooldown

As with your Battery gauge you want to always make sure to not be overcapping your Heat gauge so try to plan your Hypercharge uses accordingly.
