---
authors:
        - Robinaite
tags:
    - DNC
---
# Dancer

## What is a Dancer?

Inured to the hardships of the road, these dancers have learned to land throwing weapons with the same exacting precision as their footfalls, removing any who would obstruct the endless beat of the dance.

Dancer ({{ DNC }}) is a Physical Ranged DPS which is highly mobile. It uses dances, and can give a dance partner to someone, to share buffs to the other player and the whole party.

Dancer uses chakrams as their weapon.

## How to unlock Dancer

1. Own the Shadowbringers Expansion
2. Level any other Job to level 60
3. Start the quest [Shall we Dance](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/1b1d77bef99/){target=_blank} in Limsa Lominsa
4. Dancer starts at level 60

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("U0ytlHXvpDc")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}

## Commonly asked questions
