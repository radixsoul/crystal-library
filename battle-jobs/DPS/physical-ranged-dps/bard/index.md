---
authors:
        - Reiko
        - Robinaite
tags:
    - BRD
    - ARC
---
# Bard

## What is a Bard?

Bards trace their origins back to the bowmen of eld, who sang in the heat of battle to fortify the spirits of their companions.
In time, their impassioned songs came to hold sway over the hearts of men, inspiring their comrades to great feats and granting peace unto those who lay upon the precipice of death.

Bard ({{ BRD }}) is a Physical Ranged DPS which uses songs to empower other players.

Bard uses a bow as their weapon.

## How to unlock Bard

1. Unlock Archer in New Gridania, which starts at level 1 ([Way of the Archer](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/0754ced391b/){target=_blank})
2. Level up Archer to level 30
3. Unlock {{ BRD }} through doing the job quests. ([A Song of Bards and Bowmen](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/e8c7133b92a/){target=_blank})

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("MVPkubT--qg")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}

## Commonly asked questions

### What is Bard's overall playstyle?

Bard is a high APM (Actions Per Minute) job that relies on a priority and proc RNG system rather than a strict rotation like other jobs. Most if its time is spent maintaining its two Damage over Time (DoT) skills, while spamming various other Weaponskills and frequently weaving in many off Global Cooldown Abilities. Its overall personal damage is on the lower end of the scale but it makes up for some of that with party buffs and utility.

### What is Bard's stat priority?

Bard's main stat is **Dexterity** and as with other jobs if you are not working on your actual Best in Slot gearset, the first rule is to maximize your main stat which essentially means go for the highest item level as first priority. After that the order goes **Critical Hit > Direct Hit >= Determination > Skill Speed**. Also note, that although it is the bottom priority, Bard, unlike the other Phys Ranged DPS does benefit from a small amount of Skill Speed, usually going for between 2.48 and 2.46 recast time.

### What is the order I should be using my songs?

- Single Target: Wanderer's Minuet -> Mage's Ballad -> Army's Paeon. Unless fight mechanics/timing dictate otherwise, you will use the full duration of WM and MB but clip AP at 20 seconds in to get back into WM
- AoE, >6 targets (in order of strength): Mage's Ballad -> Army's Paeon -> Wanderer's Minuet
- AoE, 3-6 targets (in order of strength): Mage's Ballad -> Wanderer's Minuet -> Army's Paeon

AoE is shown in order of strength rather than in the order you should always use them in because you may want a weaker song up while your tank is pulling everything so you can still do some extra damage and charge Apex Arrow. But you wouldn't want to be using your best song available until they are all grouped up and you've stopped moving

### Should I be using my DoTs during dungeon pulls/on adds?

Yes! More DoTs equals more procs, more procs equals more DPS for Bard. For dungeon pulls, DoT enemies as your tank is pulling while having a weaker song going. Once your tank has stopped and grouped them up, swap to your better song and start spamming Quick Nock and weaving in Rain of Death procs. If enemies are living long enough, refresh some of their DoTs with Iron Jaws, 2 or 3 is generally enough at this point.
For boss adds, as long as they will live long enough for the full duration and there aren't that many, you should double DoT each one. For weaker adds it may still be worth it to single DoT a few for the extra potential procs.

### When should I use Apex Arrow?

For Single Target, the answer 99% of the time is as soon as possible once it hits 100. The most common exceptions to this are if you have a +10% or better buff(s) on that are about to fall off you can use it early at 95 for an overall gain. You can also delay using it at 100 up to 2 GCDs if Raging Strikes is about to come off cooldown.
For AoE, you should prioritize hitting more enemies than always maximizing the charge. So using it below 100 is fine if it means you'll hit more targets before they die.

### Does every target in range need to have both DoTs on them to get the full potency of Shadowbite?

No, only the enemy you are actively targeting at the time needs to have both DoTs on for all enemies that are hit by Shadowbite to receive the full potency
