---
authors:
        - Robinaite
tags:
    - RPR
---
# Reaper

## What is a Reaper?

*Job is not available yet, only with Endwalker launch!*

Reaper is a Melee DPS.

Reaper uses a scythe as their weapon

## How to unlock Reaper

1. Own the Endwalker Expansion
2. Level any other Job to level 70
3. Start the quest ????
4. Reaper starts at level 70

## Common asked questions about Reaper
