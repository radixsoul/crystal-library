---
authors:
        - Robinaite
tags:
    - MNK
    - PGL
---
# Monk

## What is a Monk?

Monks, ascetic warriors as dreaded by foes on the field of battle as the city-state's great pikemen.

Monk ({{ MNK }}) is a Melee DPS.

Monk uses fists as their weapon.

## How to unlock Monk

1. Unlock Pugilist in Ul'dah, which starts at level 1 ([Way of the Pugilist](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/3f9ca1c728d/){target=_blank})
2. Level up Pugilist to level 30
3. Unlock {{ MNK }} through doing the job quests. ([Brother from Another Mother](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/4480af45c8c/){target=_blank})

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("9FBgY7xBFQE")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}

## Commonly asked questions
