---
authors:
        - Robinaite
        - Reiko
tags:
    - WAR
    - MRD
---
# Warrior

## What is a Warrior?

Wielding greataxes and known as warriors, these men and women learn to harness their inner-beasts and translate that power to unbridled savagery on the battlefield.

Warrior ({{ WAR }}) is a tank.

Warrior uses a great axe as their weapon.

## How to unlock Warrior

1. Unlock Marauder in Gridania, which starts at level 1 ([Way of the Marauder](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/66d611f22e0/){target=_blank})
2. Level up Marauder to level 30
3. Unlock {{ WAR }} through doing the job quests. ([Pride and Duty (Will Take You from the Mountain)](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/75e8ccec1f4/){target=_blank})

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("UDUzsguKt-I")}}

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}

## Commonly asked questions

### What is Warrior's playstyle like?

Warrior is the slowest of the 4 tanks and has one of the simplest rotations. You will mainly be keeping a damage buff up, doing your 3 part combo to build your gauge and then spending that gauge on harder hitting weaponskills. You also have a devastating burst phase that allows you to use all your skills that would normally cost gauge for free and makes every hit a Critical Direct Hit for a short time. Warrior is also the best at self healing, having numerous self healing skills all of which are oGCD so you never have to slow down your damage to heal yourself.

### What about stats for WAR? Is it different from other tanks? What skill speed does it need?

Until you’re at endgame , prioritize item level above all else to maximize your main stat (which is Strength). When you are ready to meld materia and build toward your BiS your priority will be **Critical Hit > “Comfy” Sks > Determination > Tenacity**. You may notice that Direct Hit is missing, this is because of Inner Release and WAR’s other skills to guarantee so many Critical Direct Hits. Because unlike Crit, Direct Hit does not increase its modifier, it becomes worth much less and so the flat DPS buff of Determination is better. This makes it markedly different from the other tank meld priorities.

As for what is “Comfy” skill speed: This can vary but a good target is 2.38 GCD recast time. This makes it easy to hit 5 Fell Cleaves during Inner Release (very important), and it is the best for alignment of Inner Release with your other GCDs. However, WAR can operate at as slow as 2.43 seconds and as fast as 2.36 and still be very effective and those other speeds can have advantages and disadvantages. Consult The Balance for more details or check the [Tank Gear Manager](https://docs.google.com/spreadsheets/d/1E_Lhl0urAwXts1O4vzBiZMZnKYxa0Kz0W7r1bJyNDgo/edit#gid=772715780) to see what kind of dps you will get with different combos of gear/melds

### Why would I use Upheaval when Fell Cleave is stronger?

While it is true that Fell Cleave’s potency is greater than Upheaval's, its potency per unit of Beast Gauge is actually quite a bit lower. Upheaval = 450/20 = 22.5 potency per gauge. Fell Cleave = 590/50 = 11.8, close to half what Upheaval’s is. Additionally, Upheaval is an off Global Cooldown skill vs Fell Cleave is a GCD. Meaning that even if Upheaval was woven with your weakest GCD, it would still be a greater overall potency per GCD than Fell Cleave.

### When should I use Onslaught?

Always during Inner Release since it’s free. Outside of IR, you only want to use it if you are far enough away that it will gain you a GCD use. Otherwise, it’s a waste of Beast Gauge.

### Is Raw Intuition or Nascent Flash better to use?

If you’re dealing with multiple targets and using AoE, the answer is always Nascent Flash (NF) by a huge margin. For bosses, the answer is still usually going to be NF. Simply put, unless the attack will kill you outright without the mitigation of Raw Intuition (RI), NF (under most circumstances) will be able to heal you for more damage than RI would have mitigated. Consult the guide on the Balance if you want to get into the details of the math on this.

