---
authors:
        - Robinaite
tags:
    - SCH
    - ACN
---
# Scholar

## What is a Scholar?

These learned men and women defended the freedom of their tiny nation with their unique command over spell-weaving faeries, utilizing the creatures' magicks to heal the wounded and bolster the strength of their allies.

Scholar ({{ SCH }}) is a shield healer. Of the Healers, scholar focuses on preventive healing and shielding, while using a pet as auxiliary healing tool.

Scholar uses a book as their weapon.

Scholar and Summoner share the same levels, leveling one, levels the other.

## How to unlock Scholar

1. Unlock Arcanist in Limsa Lominsa which starts at level 1 ([Way of the Arcanist](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/4100af4d4b8/){target=_blank})
2. Level up Arcanist to level 30
3. Unlock {{ SCH }} through doing the job quests. ([Forgotten but Not Gone](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/2f077612950/){target=_blank})

Make sure you have done {{ MSQ }} level 20 quest, Sylph Management. For more info check here: [Advancement Job Quest does not appear](../../I-am-level-30-why-no-job-advancement-quest.md)

## Leveling

Here is a great guide on tips and rotations during leveling:
{{yt("A3umoCfgbNg")}} (Levels 1-30 are done on the Summoners guide with Arcanist)

## Endgame BiS and Rotation

For endgame best in slot gear and rotation, head over to The Balance discord server you can find here: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}

## Commonly asked questions
