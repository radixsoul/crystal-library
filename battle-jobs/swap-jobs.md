---
authors:
        - Robinaite
tags:
    - Swap Jobs
    - Gearsets
---
# How do I swap jobs?

## Short Answer

You swap jobs by changing your main weapon. If its a Job that has a class (like Lancer -> Dragoon), you also need to equip your Job Stone. 

To facilitate swapping jobs you can use gearsets. Learn more about how to do so here: [https://crystal-library.robinaite.com/user-interface/How-do-I-setup-job-change-buttons](https://crystal-library.robinaite.com/user-interface/How-do-I-setup-job-change-buttons)

If you don't have other jobs unlocked, click on the links in the menu to get to each job to see how to unlock!
