---
authors:
        - Robinaite
tags:
    - Materia
---
# What is Materia? When do I worry about it?

## Short Answer

Materia is used to increase the sub-stats of gear.

Materia is only important when you've reached max level, and are gearing for the latest gear possible. If you have materia laying around feel free to slot it into a gear slot through an NPC melder.

## Extra Info

### How do I unlock Materia?

To unlock Materia you need to do the quest [Forging the Spirit](https://ffxiv.gamerescape.com/wiki/Forging_the_Spirit){target=_blank}.

### How do I meld Materia?

To meld materia you have three options:

1. Going to an NPC Melder
2. Leveling necessary crafters
3. Asking a friend who has the necessary crafters levelled

### How do I overmeld?

Overmelding is the process of adding more materia than the base slots that the gear has. This is only possible to do on crafted (non-augmented) gear.

The overmelding process can NOT be done through an NPC Melder, and requires either having leveled your crafters or asking a friend.

When overmelding, the first Overmeld slot can be the highest Materia Tier possible. Afterwards, only one of the tier below can be used.

The more you meld, the lower the chance of success of melding it is.

You will NOT lose your gear if melding fails, you will only lose the materia you tried to add.

### Where do I get Materia?

You can get materia from:

- Doing Dungeons (only battle materia)
- Trading in Cracked Clusters
- From the marketboard
- From White Scrips (Only crafters & gatherers materia)

### What stats do I meld?

To find out what stats you should meld check The Balance discord on your job: [https://discord.gg/thebalanceffxiv](https://discord.gg/thebalanceffxiv){target=_blank}
