---
authors:
    - Reiko
tags: 
- LB
- Limit Break
---
# How does Limit Break work?

## Short Answer

Limit Break (LB) is a skill that is slowly charged via the party's actions during a duty and expended on behalf of the entire party by one player upon using the Limit Break skill. The form a Limit Break takes is determined by how many LB bars are filled (up to a max of 3) and the role of the player executing the skill: Tank, Healer, Melee DPS, Physical Ranged DPS, Magical Ranged DPS all have a different effect. To place it on a hotbar, go to the Actions & Traits menu > General

## Longer Answer

The overall strength of an LB is determined by the level (number of **full** bars of Limit Break charged at the time of use) and the combined item level of the party's weapons. This is important as it means that LB is totally unaffected by your own buffs or debuffs.

The form an LB takes is as follows:

- Tank: Partywide damage mitigation of 20%, 40%, or 80% for LB1-3 respectively. LB3 is also able to affect true damage (aka percent based damage) which is otherwise impossible to mitigate. Tank LB is instant cast so be careful not to set it off accidentally. Tank LB is almost never used outside of very specific bosses that actually require it to not wipe. However, experienced groups may want to use it for high end duties to save a run or to make certain mechanics easier.
- Healer: Partywide healing and MP restoration of 25%, 60% and 100% for LB1-3 respectively. Healer LB3 also includes a mass resurrection that brings back all party members with full health and MP and without a Weakness debuff. Healer LB has a fast cast but also comes with a fairly long animation lock especially LB3 so do be careful when using it. Healer LB is essentially only used at level 3 to resurrect a significant number of dead party members.
- Melee DPS: Massive single target damage increasing in potency as LB bars are filled
- Physical Ranged DPS: Very long and wide line AoE damage with no potency fall off. Potency increases as full LB bars increase.
- Magical Ranged DPS: Large circle AoE damage with no potency fall off. Potency increases as full LB bars increase. Caster LB has slightly higher overall potency per target than Physical Ranged
    - All DPS LBs come with a significant cast time that increases as the LB level increases as well as animation locks following execution.

It should be noted that, although it is a very common practice to save LB for a melee DPS to use against a boss (assuming you know the Tank or Healer LB is not needed), this is often not the most efficient use. If a Phys Ranged or Caster DPS can hit even 2 targets with LB, the overall potency going out is *greater* than a melee LB on a single target of that same LB level. So in dungeons or in a tight boss adds phase, it is often more ideal for a Caster or Phys Ranged to use it. However, a great many of the player population is not aware of this and may get on your case if you use it as a ranged and not let the melee use it.

## Extra Info

If you are looking to optimize the use of LB, especially melee DPS LB during higher end content like Extreme and Savage boss fights, the ideal party member to cast the LB is whichever melee DPS does the *least* damage overall as that player is going to have to stop DPSing for a good amount of time to perform the LB. This also means if at all possible, you should wait till you are not in a burst phase to use it. Also, if you have recently been resurrected and have a Weakness or Brink of Death debuff, you are the best choice to use it as LB damage is not affected by those debuffs but your normal damage of course would be.
