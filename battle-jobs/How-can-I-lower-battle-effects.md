---
authors:
        - Robinaite
tags:
    - Battle Effects
    - Spell Effects
---
# How can I lower Battle Effects?

You can lower your Spell Effects in your settings menu under: Character Configuration -> Control Settings -> Character Tab.

![Battle Effects amount Settings](Images/Battle-effects.png)

*Show Limited* will only show important effects such as the bubbles from {{WHM}} and {{SCH}}, or star from {{AST}}. Highly suggested to keep party to at least "Show Limited". Feel free to set Others to "Show None".
