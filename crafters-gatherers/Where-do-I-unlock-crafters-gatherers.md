---
authors:
        - Reiko
tags: 
    - unlock doh
    - unlock dol
    - Disciples of the Hand
    - Disciples of the Land
    - DoH
    - DoL
---
# Where can I unlock the Crafters and Gatherers?

## Short Answer

The Crafters (Disciples of the Hand, DoH) and Gatherers (Disciples of the Land, DoL) are spread out within the 3 starter cities.

Gridania is home to the Carpenters', Leatherworkers', and Botanists' guilds.

You will find the Armorers', Blacksmiths', Culinarians', and Fishers' guilds in Limsa Lominsa.

And Ul'dah is home to the Alchemists', Goldsmiths', Weavers', and Miners' guilds.

The only requirements to unlock these classes is the same for all classes outside of your starting class: Get to level 10 and finish your level 10 class quest on your starting class thus unlocking the Armoury System. For any of the classes that are found in cities other than the one you started in, you will also have to reach Level 15 in the MSQ, where it will take you on an airship to the other 2 cities.

## See Also

[When do I need to start leveling Crafters or Gatherers?](When-do-I-need-to-start-leveling-Crafters-or-Gatherers.md)
