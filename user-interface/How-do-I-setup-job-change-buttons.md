---
authors:
        - Robinaite
tags:
    - Gearsets
    - UI
---
# How do I setup job change buttons?

## Short Answer

To have job change buttons you need to make use of gearsets. You can create gearsets by clicking on the gearsets button in your character profile.

When you created a gearset you can now drag one of the items in the list.

Make sure to keep the gearsets updated by using the refresh button on the top right in your character profile.
