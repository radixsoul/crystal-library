---
authors:
        - Robinaite
tags:
    - UI
---
# User Interface

Have questions regarding User Interface in the game? Look here!

I highly suggest watching the following video from Zepla on UI:
{{yt("R0buenOiW5Q")}}

## Commonly asked questions

*You can find more questions in the menu on the left on Desktop, or clicking the menu button on the top left on mobile.*

- [How do I setup job change buttons?](How-do-I-setup-job-change-buttons.md)
