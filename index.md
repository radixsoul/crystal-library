---
description: Here you can find a lot of questions answered about the game, ranging from How to buy the game, to when do I get a mount!
authors: 
    - Robinaite
hide:
    - navigation
    - toc
---

# Crystal Library FFXIV

Welcome to the Crystal Library! A wonderful place with answers to all (well almost) your questions regarding the game. Ranging from how to unlock other classes, tips & tricks, where to find the best guides, how do I buy the game, and much more!

All content aims to easily explain or link to the necessary proper guides that wonderful people in this community made just for you!

Use the tabs above, or the search bar to search for your question. You can also use the [Tags](tags.md) page to look at all questions that can be related to each other!

Still can't find an answer to your question? Join ZeplaHQ's discord and I (Robin Naite), or some other wonderful player will help you out! Join here: [https://discord.gg/zeplahq](https://discord.gg/zeplahq){target=_blank}

## Categories

You can find questions and answers about a variety of content in this game:

- [Game Purchase](game-purchase/index.md) - Free Trial, How to buy the game, subscription information, Steam VS Windows.
- [Battle Jobs](battle-jobs/index.md) - Battle related content and pages for each job.
- [Crafters & Gatherers](crafters-gatherers/index.md) - Crafting & Gathering information, unlocks and links
- [Game Content](content/index.md) - MSQ questions, Alliance Raids, Deep Dungeons, relic Weapons, Free Company, glamour, Poetics and more!
- [User Interface](user-interface/index.md) - Guides on the User Interface, such as gearsets or other tips
- [Glossary](glossary/index.md) - Glossary of the various abbreviations used in the website
- [Tags](tags.md) - All the tags used on all the pages for easier searching
- [Contribute](contribute/index.md) - Information if you want to help or contribute to the project

## Commonly asked questions

*You can find more questions in the menu on the left on Desktop, or clicking the menu button on the top left on mobile.*

- [How do I buy the game?](game-purchase/buying-game/index.md)
- [I am Level 30 why no job advancements?](battle-jobs/I-am-level-30-why-no-job-advancement-quest.md)
- [When do I get my first mount?](content/main-scenario-quest/When-do-I-get-my-first-mount.md)
- [How do I make gil?](content/How-do-I-make-gil.md)

## About the project

The Crystal Library is a project grown out of the constant repetitive nature of answering questions in the #sprout_help channel of ZeplaHQ discord (which you can find [here](https://discord.gg/zeplahq){target=_blank}) by myself (Robin Naite). The aim is to continuously extend this knowledge base with information or links to the information regarding the game, and all the question new players, or people new to specific type of content.
The FFXIV community has made some wonderful content over the years, but its all spread over various discord servers, websites, google docs and Youtube videos. On this website I (Robin Naite) will aim to link to all this amazing information for easier searching.
  