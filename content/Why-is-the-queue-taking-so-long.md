---
authors:
        - Robinaite
tags:
    - Long Queue
---
# Why is the queue taking so long?

## Short Answer

First of all, make sure you are NOT queuing for anything of the following: Urth’s Fount”, "Extreme", "Minstrel's Ballad", "Savage", "Coils of Bahamut", or "Ultimate". These are difficult fights and you need to be prepared for them. These are normally not done through Duty Finder.

Secondly, check your Datacenter active time hours. its normal that on Mornings on working days the queue times are longer.

Thirdly, DPS will always have longer queues than healer and tanks. (10 minutes +)

Lastly, especially for people on the EU or JPN datacenters. Make sure you have all languages selected in your duty finder settings.
