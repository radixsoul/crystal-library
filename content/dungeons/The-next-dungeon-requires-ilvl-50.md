---
authors:
        - Robinaite
tags:
    - ilvl50
---
# The next dungeon requires Item Level 50. Where do I get that?

## Short Answer

To get to Item Level 50 you do:

1. Your job quests! The level 50 one will give you an Item Level 90 left-side gear coffer.
2. Buying from vendors from any of the starting cities.
