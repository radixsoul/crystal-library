---
authors:
        - Robinaite
tags:
    - Content
---
# Content

Have questions regarding any type of content in the game? Look here!

## Commonly asked questions

*You can find more questions in the menu on the left on Desktop, or clicking the menu button on the top left on mobile.*

- [How can I join a Free Company?](How-can-I-join-a-FC.md)
- [How do I apply glamour?](How-do-I-apply-glamour.md)
- [How do I get more Poetics?](How-do-I-get-more-poetics.md)
- [How do I make gil?](How-do-I-make-gil.md)
- [How do Loot drops work?](how-do-loot-drops-work.md)
