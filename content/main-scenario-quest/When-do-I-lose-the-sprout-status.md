---
authors:
        - Robinaite
tags:
    - Sprout
    - Leveling
---

# When do I lost the Sprout status?

## Short Answer

You lose the sprout status when you have 7 days of playtime and have arrived in ShB. (Both conditions need to be met)
