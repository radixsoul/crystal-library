---
authors:
        - Robinaite
tags:
    - GC
    - Leveling
---
# Which Grand Company should I pick?

## Answer

It doesn't matter! The biggest differences are colour for glamours and headquarter city. You can swap at any time as well.

Each grand company has its own headquarters, they differ at:

- Main city
- Main Colour, and thus glamour
- Frontline PvP mount

### The Maelstrom

Main city: Limsa Lominsa
Main Colour: Red

### The order of the Twin Adders

Main city: Gridania
Main Colour: Yellow

### Immortal Flames

Main city: Ul'dah
Main Colour: Black

![Grand Company Gear](images/grand-company-gear.png)
