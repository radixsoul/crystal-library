# Main Scenario Quests

Read below for more questions regarding any progress in the Main Scenario Quest

## Commonly asked questions

*You can find more questions in the menu on the left on Desktop, or clicking the menu button on the top left on mobile.*

- [When do I get my first Mount?](When-do-I-get-my-first-mount.md)
- [When do I unlock Flying?](When-do-I-Unlock-Flying.md)
- [Which Grand Company do I pick?](Which-grand-company-to-pick.md)
- [Which Quests do I prioritise?](Which-quests-do-I-prioritise.md)
