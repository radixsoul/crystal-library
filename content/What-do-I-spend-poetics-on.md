---
authors:
        - Reiko
tags:
- Spend Poetics
- Tomestones
- Gearing
- Poetics
---
# What do I spend my Poetics on?

## Short Answer

Allagan Tomestones of Poetics are primarily designed to be spent on former endgame gear for each part of the game outside the current expansion. Thus, you can get the best gear for your job for levels 50, 60, and 70 with them. To be able to purchase these sets, you will need to finish the main part of the MSQ of each expansion, except Heavensward where you simply need to reach Idyllshire. It is generally recommended, especially if it is your first level 50 job, to start with your weapon, then accessories and then armour. Alternatively, you can simply prioritize upgrading your lowest item level pieces first, and work up to your highest as you go.

## Longer Answer

Poetics have a handful of other uses outside of gearing your jobs and you might consider spending them on some of these if you find yourself with Poetics to spare

- Buying gear in advance for alt jobs before you need it
- Minions
- Orchestrion Rolls
- Crafting Materials
- Various items needed for relic weapons. The majority of these are for the Heavensward relic. However, a handful of items are needed for the A Realm Reborn relic, and 1 item for the first stage of the Shadowbringers relic
- Unidentifiable Ore which can be exchanged for Grade 3 Thanalan Topsoil which can then be sold on the Marketboard. This is the main way players try to make a bit of gil on the side from excess Poetics. Alternatively, you may also try Unidentifiable Shells for Grade 3 Shroud Topsoil if that sells better on your server

## Extra Info

!!! note Poetics Gear
    All gear bought with Poetics is now its Augmented version by default and simply costs the amount that it would have been for the base gear piece and its upgrade item.

Besides A Realm Reborn, all other expansions have 2 gear sets available for purchase with Poetics. Except for wanting them for glamour, you should always purchase the higher item level set. The sets you will want are called:

- Augmented Ironworks (A Realm Reborn, Level 50, Item Level 130)
- Augmented Shire (Heavensward, Level 60, Item Level 270)
- Augmented Scaevan (Stormblood, Level 70, Item Level 400)

Poetics vendors are found in the former endgame hubs of each part of the game (Mor Dhona, Idyllshire, Rhalgr's Reach) and now also from the Rowena's Representative NPCs which you can find near the main Aetheryte of each expansion's starter city(s).
These sets of gear will carry you through all of the patch content of their respective expansions and about halfway into the next expansion wherein you will begin to recieve better gear from the MSQ and dungeons

## See Also

- [How do I get more Poetics](How-do-I-get-more-poetics.md)
