---
authors:
        - Robinaite
tags:
    - PVP
    - Frontline
    - Rival Wings
    - Feast
---
# PvP

## Summary

There are various PvP modes in the game:

- Frontline - 24v24v24 Capture The Flag style matches
- Rival Wings - 24v24 MOBA styled matches
- Feast -  4v4 Light party pvp.

You can unlock PvP at your Grand Company location starting level 30.

## More Information

PvP in FFXIV is a whole other system, with a whole other set of abilities. Each job has their own PvP hotbars and Actions, most often similarly to their PvE counterpart.

### How to Unlock PvP

You can unlock PvP zone The Wolves' Den at one of your Grand Companies through the quest "A Pup no Longer).

To unlock Frontline & respective roulette do the quest "Like Civilized Men and Women" at your grand company

To unlock Rival Wings do the quest "Earning your Wings".

### What is Frontline?

!!! info "Amazing XP"
    Did you know you can get a lot of XP out of doing Frontline Roulette, once a day? You can even swap inside the PvP zone, and you will still get xp on the job you queued with!!

Frontline is a 3 team Capture The Flag style fight, where each team represents one of the Grand Companies. Each team consists of 24 players (3x 8 people).
Frontline has various maps with slightly different rules and spins on the Capture the Flag style game.

You can find more information regarding Frontline here: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/frontline/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/frontline/){target=_blank}

Frontline is constituted of 4 different modes:

- The Borderland Ruins (Secure) - players increase their company's tactical rating by occupying and maintaining control of key locations, as well as defeating other players in battle.
- Seal Rock (Seize) - layers aid their company by retrieving data from Allagan tomeliths, as well as defeating other players in battle.
- The Fields of Glory (Shatter) - players aid their company by retrieving data from Allagan tomeliths, as well as defeating other players in battle.
- Onsal Hakair (Danshig Naadam) -  players increase their team's tactical rating by defeating enemy players, as well as claiming ovoos (flags).

### What is Rival Wings?

Rival Wings is a large-scale PvP battle involving up to 48 combatants. Participants are divided into two teams, the Falcons and the Ravens, comprised of six light parties each.

You can find more information regarding Rival Wings here: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/rivalwings/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/rivalwings/){target=_blank}

#### Astragalos

During a Hidden Gorge campaign, teams vie for control of supplies delivered by ceruleum engines, and winning by destroying the core located on the opponent's base. (Think of League of Legends or DOTA)

More information: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/rivalwings/astragalos/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/rivalwings/astragalos/){target=_blank}

### What is The Feast?

The Feast is a PvP arena in which the main objective is to steal your opponent's medals. The winner of these matches is determined by whichever team has the most at the end of each match. Individual PvP rating and tiers will also be assigned to players and the highest-ranked participants of each season will be awarded a special prize.

You can find more information regarding The Feast here: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/wolvesden/thefeast/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/wolvesden/thefeast/){target=_blank}

### See Also

- FFXIV PvP Guide: [https://na.finalfantasyxiv.com/lodestone/playguide/pvpguide/system/](https://na.finalfantasyxiv.com/lodestone/playguide/pvpguide/system/){target=_blank}
- Revival Wings Discord -  [https://discord.gg/WYMpfYp](https://discord.gg/WYMpfYp){target=_blank}
- PvPaissa Discord - [https://discord.gg/sUy86UC](https://discord.gg/sUy86UC){target=_blank}
