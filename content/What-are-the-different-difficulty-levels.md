---
authors:
        - Robinaite
        - Reiko
tags:
    - difficulty
    - Unreal
    - Endgame
    - Savage
    - Ultimate
    - Extreme
---
# What are the different difficulty levels?

## Short Answer

The game has the following denominations for difficulty levels:

Normal content:

- (nothing)
- Hard

High-end content (ordered by least difficult to most difficult):

1. Extreme/ The Minstrel's Ballad/ Unreal
2. Savage
3. Ultimate

## Long Answer

The game has various denominations for difficulty in the game.

### Dungeons Difficulty levels

Dungeons have:

- Normal
- Hard

With hard difficulty only being a higher leveled dungeon with new areas in the same theme, it is not Harder Content.

### Trial Difficulty levels

Trials have three difficulty levels:

- Normal
- Hard (same concept as Dungeons)
- Extreme (also named The Minstrel's Ballad)
- Unreal

Extreme is the same fight but with more difficult mechanics, and is considered an entry level to High-end content.

Make sure NOT to queue these through Duty finder, and if possible watch a guide.

Unreal are older Extreme trials that have been up-synced to level 80 with a forced minimum ilvl. Currenlty, there is only a single one available which changes each major patch. In some ways Unreal are harder than Extreme trials with the forced minimum ilvl and the differnt way ARR handled mechancis back then, but in other ways they are easier as the mechanics tend to be less complex overall.

### Normal Raids Difficulty levels

Normal Raids have two difficulty levels as well:

- Normal
- Savage

Normal version is doable easily, while Savage is considerably more difficult. Compared to Extreme trials, Savage raids have more complex, less forgiving mechancis, and tighter enrage times.

### Ultimate fights

Ultimate fights are their own tier, and are considered the most difficult fights in the game.

Contrary to Savage and Extremes, these are not able to be cheesed by overlevelling or better gear, as it both:

- Syncs your level
- Syncs your item level (gear)

### Exceptions

There are a couple of fights that are not in any of the denominations above, so make sure to watch out on them:

- Binding Coils of Bahamut - {{ARR}} raid series. Some of its normal versions are on par with modern savage difficulty raids.
- Urth's Fount - It's a trial which is more difficult than normal level, but less than Extremes.
- Baldesion Arsenal & Delubrum Reginea Savage  - a 56-72 man dungeon, mostly difficult due to its gigantic player size and no resurrections. Mechanics on par with extreme level content.
- Palace of the Dead & Heaven on High - These are called Deep Dungeons, and are roguelite dungeons which the further you go into (more floors), the more difficult they become. Its highly difficult to do solo, and a nice difficulty in a party. For more read here: [Deep Dungeons](deep-dungeons/index.md)
