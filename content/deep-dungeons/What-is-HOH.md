---
authors:
        - Robinaite
tags:
    - HOH
    - Deep Dungeon
---
# What is HOH?

## Short Answer

{{HOH}} is one of the Deep Dungeons in the game. It is a roguelike dungeon crawler, with randomly generated floors, up to floor 100. It has its own gearing and leveling systems. You can do it solo, or with a party up to 4 players. The higher the floors the more difficult it gets!

It has various rewards ranging from minions, gear and mounts. It can also be used for leveling.

## Longer Answer

Heaven on High is a Deep Dungeon. Deep Dungeons are randomly generated dungeons with various floors. In the case of HoH you'll have 100 floors.

### How to unlock {{HOH}}?

You can unlock {{HOH}} by completing the quest [Knocking on Heaven's Door](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/7b7bce0bcb8/).
This requires:

1. Having done the MSQ level 63 "Tide Goes in, Imperials Go Out"
2. Completed Floor 50 of Palace of the Dead.

### How to use {{HOH}} for leveling?

HOH is used for leveling purposes to level classes from 61-70. The floors used for leveling is 21-30.

## See also

- Official Deep Dungeon Guide: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/deepdungeon2/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/deepdungeon2/){target=_blank}
- Maygi's HOH 1-100 Handbook (solo/Party): [https://docs.google.com/document/d/1YVBSTOgJO-xOAB6YyKZEZRikjXFPle6Ihf_E7VdmQnI/edit#](https://docs.google.com/document/d/1YVBSTOgJO-xOAB6YyKZEZRikjXFPle6Ihf_E7VdmQnI/edit#){target=_blank}
- Angelusdemonus Deep Dungeon Content Youtube: [https://youtube.com/c/HeavenisticChaos](https://youtube.com/c/HeavenisticChaos){target=_blank}
