---
authors:
        - Robinaite
tags:
    - POTD
    - Deep Dungeon
---
# What is POTD?

## Short Answer

{{POTD}} is one of the Deep Dungeons in the game. It is a roguelike dungeon crawler, with randomly generated floors, up to floor 200. It has its own gearing and leveling systems. You can do it solo, or with a party up to 4 players. The higher the floors the more difficult it gets!

It has various rewards ranging from minions, gear and mounts. It can also be used for leveling.

## Longer Answer

Palace of the Death is a Deep Dungeon. Deep Dungeons are randomly generated dungeons with various floors. In the case of POTD you'll have 200 floors.

### How to unlock {{POTD}}?

You can unlock {{POTD}} by completing the quest [The House That Death Built](https://na.finalfantasyxiv.com/lodestone/playguide/db/quest/83cad12ae5e/).
You need to have done MSQ quest level 17.

### How to use {{POTD}} for leveling?

POTD is used for leveling purposes to level classes up to level 60. The floors used for leveling is 51-60.

## See also

- Official Deep Dungeon Guide: [https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/deepdungeon/](https://na.finalfantasyxiv.com/lodestone/playguide/contentsguide/deepdungeon/){target=_blank}
- Maygi's POTD 1-200 Handbook (solo/Party): [https://docs.google.com/document/d/1oV_SIs5L9kD_NHO2ZsU4Tw8R6iQ4v1RC5fZPQqK6cD8/edit](https://docs.google.com/document/d/1oV_SIs5L9kD_NHO2ZsU4Tw8R6iQ4v1RC5fZPQqK6cD8/edit){target=_blank}
- Angelusdemonus Deep Dungeon Content Youtube: [https://youtube.com/c/HeavenisticChaos](https://youtube.com/c/HeavenisticChaos){target=_blank}
