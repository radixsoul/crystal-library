---
authors:
        - Robinaite
tags:
    - Unsync
---
# How do I unsync a fight?

## Short Answer

Unsynced fights, are fights that are done using the "Undersized Party" option in the duty finder settings. This will allow for doing fights solo or with any size party, and will not sync down your levels down to the fights level.

This is often done to farm older expansion content, like Extremes and Savages, as they become easier doing so.

## Extra Info

Blue Mages NEED to have this option enabled if they want to get into any dungeon or trial in the game. They can also enable Synced level, which will still allow you to go in with a different than standard party formation, but will level sync you down to respective content.
