---
authors:
        - Reiko
tags:
    - Crystal Tower
    - Alliance Raids
---
# How do I unlock The Crystal Tower?

## Short Answer

Once you have completed the final quest of the main part of A Realm Reborn, The Ultimate Weapon, numerous unlock quests will be available to you. The one you want is titled "Legacy of Allag" which is given by an "Outlandish Man" in Mor Dhona at coordinates X:21.8, Y:8.1. You must complete all 10 quests in this chain, which includes unlocking and clearing the 3 alliance raids of A Realm Reborn in order to reach Heavensward.

## Extra Info

Once you have unlocked your second alliance raid, Syrcus Tower, you will have access to the Alliance Raid Roulette which gives a decent amount of tomestones and exp for its daily bonus.
