---
authors:
        - Robinaite
tags:
    - Gil Cap
    - Free Trial
---

# What do I spend gil on? I am capped

## Short Answer

As you don't have access to the Marketboard here's a quick list of things you can spend gil on.

- 99x Gysahl Greens
- 99x Any Food from vendor
- Dyes
- Minions
- Glamour
- and more (check Long Answer)

You can find a full list of items you can buy on the Free Trial by u/SmoreOfBabylon here: [https://docs.google.com/document/d/e/2PACX-1vTHChdF-HRisOli43NPdmgZrdDcrEDxUqIy3RyoFkuZKtFrGd722hg7PvooOqhPHqk7Fa9qZGktd6li/pub](https://docs.google.com/document/d/e/2PACX-1vTHChdF-HRisOli43NPdmgZrdDcrEDxUqIy3RyoFkuZKtFrGd722hg7PvooOqhPHqk7Fa9qZGktd6li/pub){target=_blank}
