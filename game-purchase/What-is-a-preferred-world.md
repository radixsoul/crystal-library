---
authors:
        - Reiko
tags: 
- Preferred World
- Server Status
- Road to 70
---
# What is a "preferred world"?

## Short Answer

A preferred world or preferred server is one that has slightly less population than the other worlds in that data center. Thus, it is a way for the game to encourage new players to choose that world over the others. It comes with several perks for new characters created during that time, most notably a special buff the doubles your Exp gain from all sources until you reach level 70.

Other bonuses for new characters on preferred worlds:

- 10 silver chocobo feathers which can be spent on low to mid level gear to help the gearing process as you level.
- 15 extra days worth of playtime upon reaching level 30 with your first class

Preffered world status will last as long as at least the current patch.

## Extra Info

The Road to 70 Exp buff lasts for 90 days from time of character creation or until the preferred status is lifted from your world, whichever is *longer*.
The buff itself is even more powerful than many think because it doesn't just multiply the Exp you would normally recieve by 2. It does so after any and all other bonuses are applied to said Exp. It also doubles quest Exp which most all other bonuses do not. Another thing that makes it special is it applies to both battle Exp and crafter and gatherer Exp which is also very uncommon.
