---
authors:
        - Robinaite
tags:
    - Buy MAC
    - Upgrade MAC
---
# How do I buy the game for Mac?

## Requirements

You need to be on the Mac Free Trial. You can check where your free trial is in [How do I create a Free Trial Account?](../free-trial-account/index.md)

If you are on the **STEAM, Windows or PlayStation Free trial**, please look here for the respective guides: [How to Buy the game](index.md)

## Steps to buy

1. Go to your regions' Square Enix Store.
2. Buy the Complete Edition, make sure to set it to Mac PC Download.
3. Wait on the email, or check Bought Products page for the Code
4. Now go to the Mogstation here: [https://secure.square-enix.com/account/app/svc/mogstation/](https://secure.square-enix.com/account/app/svc/mogstation/){target=_blank}
5. Login with the same account you use to login into the game
6. Click "Transfer to Regular Service"
7. Input the code you saw on Step 3.
8. Continue following the steps and accept the terms.
9. Congrats, now launch the game, login, and let it download the missing expansions!
