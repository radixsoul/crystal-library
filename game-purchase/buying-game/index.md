---
authors:
        - Robinaite
tags:
    - Buy Game
---
# How do I buy the game?

## Short Answer

To get access to all currently available content and expansions you buy the **Complete edition**!

If you are on steam free trial, buy it on steam, if you are on the Windows(non-steam) Free trial, you can choose either steam or Windows (Square Enix Store). Or buy it on the PSN store for PlayStation platforms.

The moment you buy the game, and have activated it on your account, you will NEED to start paying a subscription after the free 30 days included in the Starter Edition.

Look further down below for a step by step guides on where to buy the game for each platform.

## Platforms

- [Windows version (non-steam)](Windows.md)
- [Steam version](Steam.md)
- [Mac](Mac.md)
- Playstation - Open the PSN Store and buy it through there.

## Issues & Answers

### What edition should I buy?

Check the following post: [What does each edition include?](What-does-each-edition-include.md)

### Its not accepting the code

Double check if you bought the correct editions.

If you have bought only:

- Shadowbringers
- Pre-ordered Endwalker

You will now also need to buy:

- Starter Edition

Buy this edition and use the code obtained in the same steps of your platform of choice, then afterwards use the other codes.

### I Pre-ordered Endwalker but I can't access Stormblood and Shadowbringers

You will only get access to Stormblood and Shadowbringers when Endwalker launches the 19th of November of 2021.

If you want to play Stormblood and Shadowbringers now you either:

1. Buy Shadowbringers, if you already have the Starter Edition
2. Buy the Complete Edition, if you are currently on the free trial.

## See also

You can find more info on the differences between the Steam and Square Enix Store versions here:  [What are the differences between steam and Windows versions](What-are-the-differences-between-steam-and-Windows-versions.md)
