---
authors:
        - Robinaite
tags:
    - Buy Steam
    - Upgrade Steam
---
# How do I buy the game on Steam?

## Requirements

You need to be on the Windows or Steam Free Trial. You can check where your free trial is in [How do I create a Free Trial Account?](../free-trial-account/index.md)

If you are on the Windows(non-steam), MAC, PlayStation Free trial, please look here for the respective guides: [How to Buy the game](index.md)

## Steps to buy

1. Go to: [https://store.steampowered.com/app/39210/FINAL_FANTASY_XIV_Online/](https://store.steampowered.com/app/39210/FINAL_FANTASY_XIV_Online/){target=_blank}
2. Buy the Complete Edition.
3. Go to your Library
4. Right Click Final Fantasy XIV
5. Click Manage, then CD Keys
6. You should see 2 keys
7. Now go to the Mogstation here: [https://secure.square-enix.com/account/app/svc/mogstation/](https://secure.square-enix.com/account/app/svc/mogstation/){target=_blank}
8. Login with the same account you use to login into the game
9. Click "Transfer to Regular Service"
10. Input the first code you saw on Step 6. (Do Not worry if Steam is greyed out, just click windows and use the code)
11. Continue following the steps and accept the terms to ugprade to Staterter Edition.
12. When finished, Click on "Your Account"
13. Click the big red button to add Expansion to your account
14. Copy the second code and put it in.
15. Follow the steps
16. Congrats, now launch the game, login, and let it download the missing expansions!