# Game Purchases and Subscriptions info

Do you have any questions around the free trial or buying the game? Check the pages in this category!

## Commonly asked questions

*You can find more questions in the menu on the left on Desktop, or clicking the menu button on the top left on mobile.*

- [Creating a Free Trial Account](free-trial-account/index.md)
- [How do I buy the game?](buying-game/index.md)
- [Which server should I pick?](Which-server-should-I-pick.md)
