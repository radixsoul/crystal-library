---
authors:
        - President Tag
        - Robinaite
tags:
    - PS Free Trial
    - Account Issues
---
# How do I create a Free Trial account on PlayStation?

## Preparations

If you do not have a Square Enix account then you can continue with creating the account.

If you have already have a Square Enix Account or a Free Trial Account make sure to check Prepararations in: [How to create a free trial account](index.md)

## Creating the Account

1. Open the PlayStation Store
2. Search for Final Fantasy XIV Free Trial
3. Download it.
4. Follow the steps it indicates.
